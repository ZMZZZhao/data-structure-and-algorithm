#define _CRT_SECURE_NO_WARNINGS

#include<stdio.h>

//二叉树的链式结构
typedef int BTDataType;
typedef struct BinaryTreeNode
{
	BTDataType data;
	struct BinaryTreeNode* left;
	struct BinaryTreeNode* right;

}BTNode;

BTNode* BuyNode(BTDataType x)
{
	BTNode* node = (BTNode*)malloc(sizeof(BTNode));
	node->data = x;
	node->left = NULL;
	node->right = NULL;

	return node;
}
//自建二叉树
BTNode* CreatBinaryTree()
{
	BTNode* node1 = BuyNode('A');
	BTNode* node2 = BuyNode('B');
	BTNode* node3 = BuyNode('C');
	BTNode* node4 = BuyNode('D');
	BTNode* node5 = BuyNode('E');
	BTNode* node6 = BuyNode('F');
	BTNode* node7 = BuyNode('G');

	node1->left = node2;
	node1->right = node3;
	node2->left = node4;
	node3->left = node5;
	node3->right = node6;
	node4->left = node7;
	return node1;
}

////二叉树前序遍历
//void PreOrder(BTNode* root) {
//	if (root == NULL)
//	{
//		printf("NULL ");
//		return;
//	}
//
//	printf("%c ", root->data);
//	PreOrder(root->left);
//	PreOrder(root->right);
//}
//
//// 二叉树中序遍历
//void InOrder(BTNode* root)
//{
//	if (root == NULL) {
//		printf("NULL ");
//		return;
//	}
//
//	InOrder(root->left);
//	printf("%c ", root->data);
//	InOrder(root->right);
//}

//// 二叉树后序遍历
//void PostOrder(BTNode* root)
//{
//	if (root == NULL) {
//		printf("NULL ");
//		return;
//	}
//
//	PostOrder(root->left);
//	PostOrder(root->right);
//	printf("%c ", root->data);
//}
// 
// 
// 求二叉树节点个数
////1、遍历(前序)  -- 全局变量
//int size = 0;
//void BinaryTreeSize(BTNode* root)
//{
//	if (root == NULL)
//		return;
//	else
//		++size;
//
//	BinaryTreeSize(root->left);
//	BinaryTreeSize(root->right);
//}

// 2、遍历(前序)  -- 局部变量
//void BinaryTreeSize(BTNode* root, int* psize)
//{
//	if (root == NULL)
//		return;
//	else
//		++(*psize);
//
//	BinaryTreeSize(root->left, psize);
//	BinaryTreeSize(root->right, psize);
//}

// 3.分治
//int BinaryTreeSize(BTNode* root)
//{
//	return root == NULL ? 0 : 1
//		+ BinaryTreeSize(root->left)
//		+ BinaryTreeSize(root->right);
//}

// 二叉树叶子节点个数
//int BinaryTreeLeafSize(BTNode* root)
//{
//	if (root == NULL)
//	{
//		return 0;
//	}
//	else if (root->left == NULL && root->right == NULL)
//	{
//		return 1;
//	}
//	else
//	{
//		return BinaryTreeLeafSize(root->left)
//			+ BinaryTreeLeafSize(root->right);
//	}
//}
//

// 二叉树第k层节点个数
//int BinaryTreeLevelKSize(BTNode* root, int k)
//{
//	if (root == NULL)
//		return 0;
//
//	if (k == 1)
//		return 1;
//
//	return BinaryTreeLevelKSize(root->left, k - 1)
//		+ BinaryTreeLevelKSize(root->right, k - 1);
//}

// 二叉树深度/高度
//int BinaryTreeDepth(BTNode* root)
//{
//	if (root == NULL)
//	{
//		return 0;
//	}
//
//	int leftDepth = BinaryTreeDepth(root->left);
//	int rightDepth = BinaryTreeDepth(root->right);
//
//	return leftDepth > rightDepth ? leftDepth + 1 : rightDepth + 1;
//}


// 二叉树查找值为x的节点
//BTNode* BinaryTreeFind(BTNode* root, BTDataType x)
//{
//	if (root == NULL)
//		return NULL;
//
//	if (root->data == x)
//		return root;
//
//	BTNode* retLeft = BinaryTreeFind(root->left, x);
//	if (retLeft)
//	{
//		return retLeft;
//	}
//
//	BTNode* retRight = BinaryTreeFind(root->right, x);
//	if (retRight)
//	{
//		return retRight;
//	}
//
//	return NULL;
//
//}

int main()
{
	// 求二叉树节点个数
	

	////1.全局--缺点：第二次使用(例如查看另一颗树节点个数)，size值会继承上一次
	//BTNode* root = CreatBinaryTree();
	//BinaryTreeSize(root);
	//printf("BinaryTreeSize:%d\n", size);
	//BinaryTreeSize(root);
	//printf("BinaryTreeSize:%d\n", size);

	//2.局部--传参时注意不能使用传值传参
	//BTNode* root = CreatBinaryTree();
	//int size1 = 0;
	//BinaryTreeSize(root, &size1);
	//printf("BinaryTreeSize:%d\n", size1);
	//int size2 = 0;
	//BinaryTreeSize(root, &size2);
	//printf("BinaryTreeSize:%d\n", size2);
	
	
	//3.分治--利用递归
	/*BTNode* root = CreatBinaryTree();
	BinaryTreeSize(root);
	printf("BinaryTreeSize:%d\n", BinaryTreeSize(root));
	BinaryTreeSize(root);
	printf("BinaryTreeSize:%d\n", BinaryTreeSize(root));*/

	//叶子节点个数
	/*BTNode* root = CreatBinaryTree();
	printf("BinaryTreeLeafSize:%d\n", BinaryTreeLeafSize(root));*/

	////第k层节点个数
	//BTNode* root = CreatBinaryTree();
	//printf("BinaryTreeLevelKSize:%d\n", BinaryTreeLevelKSize(root, 4));

	// 二叉树深度/高度
	//核心思想：max(左子树的深度，右子树的深度)+1
	/*BTNode* root = CreatBinaryTree();
	printf("BinaryTreeDepth:%d\n", BinaryTreeDepth(root));
*/

	// 二叉树查找值为x的节点

	/*BTNode* root = CreatBinaryTree();
	BTNode* ret = BinaryTreeFind(root, 'N');
	if (ret)
	{
		printf("找到了\n");
	}
	else
	{
		printf("没有找到\n");
	}
	
	return 0;*/
}