#include"Stack.h"
//��ʼ��ջ
void StackInit(ST* ps)
{
	assert(ps);
	ps->a = NULL;
	ps->top = 0;
	ps->capacity = 0;
}

//����ջ
void StackDestory(ST* ps)
{
	assert(ps);
	if (ps->a)
	{
		free(ps->a);//�ͷ�ջ
	}
	ps->a = NULL;
	ps->top = 0;//-1Ҳ���� //ջ����0
	ps->capacity = 0;//������0
}

//��ջ
void StackPush(ST* ps, STDatatype x)
{
	assert(ps);
	//���ռ乻����������������
	if (ps->top == ps->capacity)
	{
		int newcapacity = ps->capacity == 0 ? 4 : ps->capacity * 2;
		STDatatype* tmp = realloc(ps->a, sizeof(STDatatype) * newcapacity);
		if (tmp == NULL)
		{
			printf("realloc fail!\n");
			exit(-1);
		}
		ps->a = tmp;
		ps->capacity = newcapacity;
	}
	ps->a[ps->top] = x;//ջ��λ�ô��Ԫ��x
	ps->top++;//ջ������
}

//��ջ
void StackPop(ST* ps)//ɾ
{
	assert(ps);
	assert(!StackEmpty(ps));//���ջ�Ƿ�Ϊ��
	--ps->top;//ջ������
}

//���ջ�Ƿ�Ϊ��
bool StackEmpty(ST* ps)
{
	assert(ps);
	return ps->top == 0;
}

//��ȡջ����ЧԪ�ظ���
int  StackSize(ST* ps)
{
	assert(ps);
	return ps->top;
}

//��ȡջ��Ԫ��
STDatatype StackTop(ST* ps)
{
	assert(ps);
	assert(!StackEmpty(ps));//���ջ�Ƿ�Ϊ��

	return ps->a[ps->top - 1];//����ջ��Ԫ��
}