#pragma once
#include<stdio.h>
#include<stdlib.h>
#include<assert.h>
#include<stdbool.h>
typedef int STDatatype;//栈中存储的元素类型（这里用整型举例）
typedef struct Stack
{
	STDatatype* a;//栈
	int top;//栈顶
	int capacity;//容量，方便增容
}ST;
// 初始化栈
void StackInit(ST* ps);

// 销毁栈
void StackDestory(ST* ps);

// 入栈
void StackPush(ST* ps,STDatatype x);

// 出栈
void StackPop(ST* ps);

// 检测栈是否为空，如果为空返回非零结果，如果不为空返回0
bool StackEmpty(ST* ps);

// 获取栈中有效元素个数
int  StackSize(ST* ps);

// 获取栈顶元素
STDatatype StackTop(ST* ps);