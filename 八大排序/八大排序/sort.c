#define _CRT_SECURE_NO_WARNINGS

#include"sort.h"


void Print(int* a, int n)
{
	for (int i = 0; i < n ; i++)
	{
		printf("%d ", a[i]);
		
	}
	printf("\n");
}

//插入排序
void InsertSort(int* a, int n)
{
	for (int i = 0; i < n-1; i++)
	{
		//单趟插入
		int end = i;
		int tmp = a[end + 1];
		while (end >= 0)
		{
			if (tmp < a[end])
			{
				a[end + 1] = a[end];
				end--;
			}
			else
				break;
		}
		a[end + 1] = tmp;
	}

}


//希尔排序
void Shellsort(int* a, int n)
{
	int gap = n;
	while (gap > 1)
	{
		gap = gap/3 + 1;
		for (int i = 0; i < n - gap; i++)
		{
			int end = i;
			int tmp = a[end + gap];
			while (end >= 0)
			{
				if (tmp < a[end])
				{
					a[end + gap] = a[end];
					end -= gap;
				}
				else
					break;
				

			}
			a[end + gap] =tmp;
		}
	}
}


void swap(int* pa, int* pb)
{
	int temp = *pa;
	*pa = *pb;
	*pb = temp;
}
//选择排序
void SelectSort(int* a, int n)
{
	int begin = 0;
	int end = n - 1;
	while (begin<end)
	{
		int mini = begin;
		int maxi = begin;
		for (int i = begin; i <=end; i++)
		{
			
			if (a[i] < a[mini])
			{
				mini = i;
			}
			if (a[i] > a[maxi])
			{
				maxi = i;
			}
		}
		swap(&a[begin], &a[mini]);
		if (begin == maxi)
		{
			maxi = mini;
		}
		swap(&a[end], &a[maxi]);
		++begin;
		--end;
	}

}

//向下调整算法
void AdjustDown(int* a, int n, int parent)
{
	int child = parent * 2 + 1;
	while (child < n)
	{
		if (child + 1 < n && a[child + 1] > a[child])
		{
			child = child + 1;
		}
		if (a[child] > a[parent])
		{
			swap(&a[child], &a[parent]);
			parent = child;
			child = parent * 2 + 1;
		}
		else
			break;
	}
}


// 堆排序 O(N*logN)
void HeapSort(int* a, int n)
{
	//建堆
	
	for (int i = (n - 1 - 1) / 2; i >= 0; i--)
	{
		AdjustDown(a,n-1,i);
	}
	//排序
	int end = n - 1;
	while (end > 0)
	{
		swap(&a[0], &a[end]);
		AdjustDown(a, end, 0);
		end--;
	}
}

//冒泡排序
//法1
//void BubbleSort(int* a, int n)
//{
//	for (int j = 0; j < n; j++)
//	{
//		for (int i = 0; i < n-j-1; i++)
//		{
//			if (a[i] > a[i + 1])
//			{
//				swap(&a[i], &a[i + 1]);
//			}
//		}
//	}
//}

//法2
//void BubbleSort(int* a, int n)
//{
//	for (int j = n; j > 0; j--)//控制终止条件
//	{
//		for (int i = 0; i <j-2; i++)
//		{
//			if (a[i] > a[i + 1])
//			{
//				swap(&a[i], &a[i + 1]);
//			}
//		}
//	}
//}

//优化--以法2为例
//void BubbleSort(int* a, int n)
//{
//	int exchange = 0;
//	for (int j = n; j > 0; j--)//控制终止条件
//	{
//		for (int i = 0; i < j - 2; i++)
//		{
//			if (a[i] > a[i + 1])
//			{
//				swap(&a[i], &a[i + 1]);
//				exchange = 1;
//			}
//		}
//		if (exchange == 0)//一趟下来，如果enchange==0,说明原序列已经是升序，不用接下来比较，直接退出
//		{
//			break;
//		}
//	}
//}


//快排--Hoare版本

//（单趟排序）
//int PartSort1(int* a, int left, int right)
//{
//	
//	int keyi = left;
//	while (left < right)
//	{
//		// 左边做key，右边先走找小
//		while (left < right && a[right] >= a[keyi])
//		{
//			--right;
//		}
//
//		// 左边再走，找大
//		while (left < right && a[left] <= a[keyi])
//		{
//			++left;
//		}
//
//		// 交换，把大的换到右边，小的换到左边
//		swap(&a[left], &a[right]);
//	}
//
//	swap(&a[keyi], &a[left]);
//
//	return left;
//}
//
////递归
//void QuickSort(int* a, int left, int right)
//{
//	if (left >= right)
//		return;
//
//	int keyi = PartSort1(a, left, right);
//	
//	QuickSort(a, left, keyi - 1);
//	QuickSort(a, keyi + 1, right);
//}
//
//
//void TestQuickSort()
//{
//	int	a[] = { 6,1,2,7,9,3,4,5,10,8 };
//	int n = (sizeof(a) / sizeof(int));
//	Print(a, n);
//	QuickSort(a, 0, sizeof(a) / sizeof(int)-1 );
//	
//	Print(a, n);
//}

//Hoare版本  --理想的时间复杂度是(N*logN),如果原序列本身是有序的,即最坏情况，那么就变为了N^2,怎么办呢

//优化

//////////////////////////////////////////////////////////////////////////////////////////////////////////////
// 三数取中(取下标left right,mid中大小为中间的数)

//有了三数取中法，即使有序也不会出现时间复杂度为N^2的最坏情况
//int GetMidIndex(int* a, int left, int right)
//{
//	//int mid = (left + right) / 2;
//	int mid = left + (right - left) / 2;
//	if (a[left] < a[mid])
//	{
//		if (a[mid] < a[right])
//		{
//			return mid;
//		}
//		else if (a[left] < a[right])
//		{
//			return right;
//		}
//		else
//		{
//			return left;
//		}
//	}
//	else  //(a[left] > a[mid])
//	{
//		if (a[mid] > a[right])
//		{
//			return mid;
//		}
//		else if (a[left] < a[right])
//		{
//			return left;
//		}
//		else
//		{
//			return right;
//		}
//	}
//
//}
//
//int PartSort1(int* a, int left, int right)
//{
//	int midi = GetMidIndex(a, left, right);
//	swap(&a[left], &a[midi]);
//
//	int keyi = left;
//	while (left < right)
//	{
//		// 左边做key，右边先走找小
//		while (left < right && a[right] >= a[keyi])
//		{
//			--right;
//		}
//
//		// 左边再走，找大
//		while (left < right && a[left] <= a[keyi])
//		{
//			++left;
//		}
//
//		// 交换，把大的换到右边，小的换到左边
//		swap(&a[left], &a[right]);
//	}
//
//	swap(&a[keyi], &a[left]);
//
//	return left;
//}
//void QuickSort(int* a, int left, int right)
//{
//	if (left >= right)
//		return;
//
//	int keyi = PartSort1(a, left, right);
//
//	QuickSort(a, left, keyi - 1);
//	QuickSort(a, keyi + 1, right);
//}
//
//
//void TestQuickSort()
//{
//	int	a[] = { 6,1,2,7,9,3,4,5,10,8 };
//	int n = (sizeof(a) / sizeof(int));
//	Print(a, n);
//	QuickSort(a, 0, sizeof(a) / sizeof(int) - 1);
//
//	Print(a, n);
//}

//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

// Hoare方法有不少容易出错细节
// 有人就进行了优化，对于单趟给了以下两种方式

// 1、挖坑法

//int PartSort1(int* a, int left, int right)
//{
//
//	int midi = GetMidIndex(a, left, right);
//	swap(&a[left], &a[midi]);
//
//	int key = a[left];
//	int hole = left;
//	while (left < right)
//	{
//		// 右边去找小，填到左边的坑里面
//		while (left < right && a[right] >= key)
//		{
//			--right;
//		}
//
//		// 把右边找的小的，填到左边的坑，自己形成新的坑
//		a[hole] = a[right];
//		hole = right;
//
//		// 左边去找大，填到右边的坑里面
//		while (left < right && a[left] <= key)
//		{
//			++left;
//		}
//
//		// 把左边找的大的，填到右边的坑，自己形成新的坑
//		a[hole] = a[left];
//		hole = left;
//	}
//
//	a[hole] = key;
//	return hole;
//}
//
////2.前后指针法
//int PartSort1(int* a, int left, int right)
//{
//	int midi = GetMidIndex(a, left, right);
//	swap(&a[left], &a[midi]);
//
//	int keyi = left;
//	int prev = left;
//	int cur = prev + 1;
//
//	while (cur <= right)
//	{
//		if (a[cur] < a[keyi] && ++prev != cur)
//			Swap(&a[prev], &a[cur]);
//
//		++cur;
//	}
//
//	Swap(&a[prev], &a[keyi]);
//
//	return prev;
//}

/////////////////////////////////////////////////////////////////

//快排《非递归》
//递归有缺陷--当数据量大即深度足够大时，会导致栈溢出。
//非递归-利用栈(数据结构中的栈是在堆区开辟的，堆区所占内存空间要比栈区大，一般不会导致溢出)

//void QuickSortNonR(int* a, int left, int right)
//{
//	//自己先写好栈的相关功能
//	ST st;
//	StackInit(&st);
//	StackPush(&st, right);
//	StackPush(&st, left);
//
//	while (!StackEmpty(&st))
//	{
//		int begin = StackTop(&st);
//		StackPop(&st);
//
//		int end = StackTop(&st);
//		StackPop(&st);
//
//		int keyi = PartSort3(a, begin, end);
//		//  [begin, keyi-1] keyi [keyi+1, end]
//		if (keyi + 1 < end)//右边至少还有一个数时进入条件---否则右边不需要再单趟排了
//		{
//			StackPush(&st, end);
//			StackPush(&st, keyi + 1);
//		}
//
//		if (begin < keyi - 1)//左边至少还有一个数时进入条件---否则左边不需要再单趟排了
//		{
//			StackPush(&st, keyi - 1);
//			StackPush(&st, begin);
//		}
//	}
//
//	StackDestroy(&st);
//}
//


//////////////////////////////////////////////////////////////////
//归并排序

void _MergeSort(int* a, int left, int right, int* tmp)
{
	if (left >= right)
		return;

	int mid = (right + left) / 2;
	// [left, mid][mid+1, right]
	_MergeSort(a, left, mid, tmp);
	_MergeSort(a, mid + 1, right, tmp);
	// 归并
	int begin1 = left, end1 = mid;
	int begin2 = mid + 1, end2 = right;
	int index = left;
	while (begin1 <= end1 && begin2 <= end2)
	{
		if (a[begin1] < a[begin2])
		{
			tmp[index++] = a[begin1++];
		}
		else
		{
			tmp[index++] = a[begin2++];
		}
	}

	while (begin1 <= end1)
	{
		tmp[index++] = a[begin1++];
	}

	while (begin2 <= end2)
	{
		tmp[index++] = a[begin2++];
	}

	// 归并后的结果，拷贝回到原数组
	for (int i = left; i <= right; ++i)
	{
		a[i] = tmp[i];
	}
}

void MergeSort(int* a, int n)
{
	int* tmp = (int*)malloc(sizeof(int) * n);
	_MergeSort(a, 0, n - 1, tmp);
	free(tmp);
}


void TestMergeSort()
{
	//int	a[] = { 6, 1, 2, 7, 9, 3, 4, 5, 10, 8 };
	int	a[] = { 10, 6, 7, 1, 3, 9, 4,2 };
	Print(a, sizeof(a) / sizeof(int));
	MergeSort(a, sizeof(a) / sizeof(int));
	Print(a, sizeof(a) / sizeof(int));
}