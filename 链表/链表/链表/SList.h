#define _CRT_SECURE_NO_WARNINGS
#pragma once
#include <stdio.h>
#include <stdlib.h>
#include <assert.h>
#include<stdbool.h>
typedef int SLTDataType;

typedef struct SListNode
{
	SLTDataType data;  // int val;
	struct SListNode* next;
}SLTNode;

// 要改变传过来的指向第一个节点的指针就传二级
// 不改变传过来的指向第一个节点的指针就传一级

//读写修改的函数传二级指针

void SListPushBack(SLTNode** pphead, SLTDataType x);//尾插

void SListPushFront(SLTNode** pphead, SLTDataType x);//头插

void SListPopBack(SLTNode** pphead);//尾删

void SListPopFront(SLTNode** pphead);//头删

//只读的函数接口传一级指针
SLTNode* BuySListNode(SLTDataType x);//创建新节点

void SListPrint(SLTNode* phead);//打印

int SListSize(SLTNode* phead);//查看数据元素个数

bool SListEmpty(SLTNode* phead);//判空  

SLTNode* SListFind(SLTNode* phead, SLTDataType x);//查找元素

// 在pos位置之前去插入x
void SListInsert(SLTNode** pphead, SLTNode* pos, SLTDataType x);//效率不高，得找插入前面的结点
//在pos位置后面去插入x
void SListInsertAfter(SLTNode* pos, SLTDataType x);


// 删除pos位置的值
void SListErase(SLTNode** pphead, SLTNode* pos);//删除pos位置
void SListEraseAfter(SLTNode* pos);//删除pos后面位置


void SListDestory(SLTNode** pphead);//销毁结点


