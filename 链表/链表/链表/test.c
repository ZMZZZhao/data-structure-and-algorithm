#include"SList.h"

void TestSList1()//手动创建链表
{
	SLTNode* n1 = (SLTNode*)malloc(sizeof(SLTNode));
	n1->data = 1;

	SLTNode* n2 = (SLTNode*)malloc(sizeof(SLTNode));
	n2->data = 2;

	SLTNode* n3 = (SLTNode*)malloc(sizeof(SLTNode));
	n3->data = 3;

	SLTNode* n4 = (SLTNode*)malloc(sizeof(SLTNode));
	n4->data = 4;

	n1->next = n2;
	n2->next = n3;
	n3->next = n4;
	n4->next = NULL;

	SLTNode* plist = n1;
	SListPrint(plist);
}

void TestSList2()
	{
	SLTNode* plist = NULL;
	SListPushFront(&plist, -1);//头插
	SListPrint(plist);

	SListPushBack(&plist, 1);//尾插
	SListPushBack(&plist, 2);
	SListPushBack(&plist, 3);
	SListPushBack(&plist, 4);  

	SListPrint(plist);

	SListPushFront(&plist, 0);//头插
	SListPrint(plist);

	SListPopBack(&plist);//尾删
	SListPopBack(&plist);//尾删
	SListPopBack(&plist);//尾删
	SListPopBack(&plist);//尾删
	SListPrint(plist);

	SListPopFront(&plist);//头删
	SListPrint(plist);

	printf("SListSize:%d\n", SListSize(plist));
	printf("SListEmpty:%d\n", SListEmpty(plist));

	}

TestSList3()
{
	SLTNode* plist = NULL;
	SListPushBack(&plist, 1);//尾插
	SListPushBack(&plist, 2);
	SListPushBack(&plist, 3);
	SListPushBack(&plist, 4);
	SListPushBack(&plist, 5);
	SListPrint(plist);
	int n = 0;
	printf("请输入需要查找的元素：");
	scanf("%d", &n);
	SLTNode* pos = SListFind(plist,n);//查找元素
	if (pos)
	{
		printf("找到了!\n");
		//用SListFind找到元素返回的是结点指针，,就可以修改此元素
		int m = 0;
		printf("请修改元素为:");
		scanf("%d", &m);
		pos->data = 20;
		SListPrint(plist);
	}
	else
	{
		printf("没找到!\n");
	}
	

}

TestSList4()
{
	SLTNode* plist = NULL;
	SListPushBack(&plist, 1);//尾插
	SListPushBack(&plist, 2);
	SListPushBack(&plist, 3);
	SListPushBack(&plist, 4);
	SListPushBack(&plist, 5);
	SListPrint(plist);
	int n = 0;
	printf("请输入需要查找的元素：");
	scanf("%d", &n);
	SLTNode* pos = SListFind(plist, n);//查找元素
	if (pos)
	{
		printf("找到了!\n");
		int m = 0;
		printf("请输入插入元素:");
		scanf("%d", &m);
		SListInsert(&plist,pos, m);// 在pos位置之前去插入元素
		SListInsertAfter(pos, -1);//在pos位置后面去插入元素
		SListPrint(plist);
	}
	
}


TestSList5()
{
	SLTNode* plist = NULL;
	SListPushBack(&plist, 1);//尾插
	SListPushBack(&plist, 2);
	SListPushBack(&plist, 3);
	SListPushBack(&plist, 4);
	SListPushBack(&plist, 5);
	SListPrint(plist);
	int n = 0;
	printf("请输入需要查找的元素：");
	scanf("%d", &n);
	SLTNode* pos = SListFind(plist, n);//查找元素
	if (pos)
	{
		printf("找到了!\n");
		SListEraseAfter(pos);
		SListPrint(plist);

		SListErase(&plist, pos);
		SListPrint(plist);
	}
	else
	{
		printf("没有此元素！\n");
	}
	SListDestory(&plist);//销毁结点
	SListPrint(plist);
}

int main()
{
	//TestSList1();
	//TestSList2();
	//TestSList3();//查找元素并修改元素

	//TestSList4();//在pos位置之前之后插入元素
	//单链表不适合在pos的位置之前插入元素，因为需要找前一个位置
	TestSList5();//删除pos位置元素
	return 0;
}