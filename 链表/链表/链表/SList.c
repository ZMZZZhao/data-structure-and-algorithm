#include"SList.h"

void SListPrint(SLTNode* phead)
{
	SLTNode* cur = phead;
	while (cur != NULL)
	{
		printf("%d->", cur->data);
		cur = cur->next;
	}
	printf("NULL\n");
}

SLTNode* BuySListNode(SLTDataType x)//创建新节点
{
	SLTNode* node = (SLTNode*)malloc(sizeof(SLTNode));
	if (node == NULL)
	{
		printf("malloc fall!\n");
		exit(-1);
	}
	node->data = x;
	node->next = NULL;
	return node;
}
void SListPushBack(SLTNode** pphead, SLTDataType x)//尾插
{
	assert(pphead);

	if (*pphead == NULL)
	{
		SLTNode* newnode = BuySListNode(x);
		*pphead = newnode;
	}
	
	else
	{
		//找尾
		SLTNode* tail = *pphead;
		while (tail->next != NULL)
		{
			tail = tail->next;
		}
		SLTNode* newnode = BuySListNode(x);
		tail->next = newnode;
	}

}

void SListPushFront(SLTNode** pphead, SLTDataType x)//头插
{
		SLTNode* newnode = BuySListNode(x);
		newnode->next = *pphead;
		*pphead = newnode;
}

void SListPopBack(SLTNode** pphead)//尾删
{
	assert(pphead);

	//没有结点断言报错 -- 处理比较激进
	assert(*pphead);
	//1.一个结点
	//2.多个结点
	if ((*pphead)->next == NULL)
	{
		free(*pphead);
		*pphead = NULL;
	}
	else
	{
		SLTNode* prev = NULL;
		SLTNode* tail = *pphead;
		while (tail->next != NULL)
		{
			prev = tail;
			tail = tail->next;
		}
		free(tail);
		prev->next = NULL;
	}
	
}

void SListPopFront(SLTNode** pphead)//头删
{
	//plist一定有地址，所以pphead不为空
	assert(pphead);

	//1.链表为空
	assert(*pphead);
	//2.只有一个结点
	//3.多个结点
	SLTNode* newnode = (*pphead)->next;
	free(*pphead);
	(*pphead) = newnode;
}

int SListSize(SLTNode* phead)//查看数据元素个数
{
	int size = 0;
	SLTNode* cur = phead;
	while (cur)
	{
		size++;
		cur = cur->next;
	}
	return size;
}

//bool 只有真和假
bool SListEmpty(SLTNode* phead)//判空
{
	//法一
	//return phead == NULL;//空为真，非空为假，用（0，1）表示
	//法二
	return phead == NULL ? true : false;//空为真，非空为假，用（0，1）表示
}

SLTNode* SListFind(SLTNode* phead, SLTDataType x)//查找元素
{
	SLTNode* cur = phead;
	while (cur)
	{
		if (cur->data == x)
		{
			return cur;
		}
		else
		{
			cur = cur->next;
		}
		
	}
	return NULL;
}

void SListInsert(SLTNode** pphead, SLTNode* pos, SLTDataType x)// 在pos位置之前去插入x
{
	assert(pphead);
	assert(pos);

	// 1、头插
	
	if (*pphead == pos)
	{
		SListPushFront(pphead, x);
	}
	// 2、后面插入
	else
	{
		// 找到pos位置的前一个节点
		SLTNode* prev = *pphead;
		while (prev->next != pos)
		{
			prev = prev->next;
		}

		SLTNode* newnode = BuySListNode(x);
		newnode->next = pos;
		prev->next = newnode;
	}
}

void SListInsertAfter(SLTNode* pos, SLTDataType x)//在pos位置后面去插入x
{
	assert(pos);
	SLTNode* newnode = BuySListNode(x);
	//下面两行代码的顺序不能换
	newnode->next = pos->next;
	pos->next = newnode;

}


void SListErase(SLTNode** pphead, SLTNode* pos)
{
	assert(pphead);
	assert(pos);
	//1.头删
	if (pos== *pphead)
	{
		SListPopFront(pphead);
	}
//后面结点删除
	else
	{
		//找pos的前一个结点
		SLTNode* prev =* pphead;
		while (prev->next!=pos)
		{
			prev = prev->next;
		}
		prev->next = pos->next;
		free(pos);
		pos = NULL;
	}
}

void SListEraseAfter(SLTNode* pos)
{
	assert(pos);
	if (pos->next != NULL)
	{
		SLTNode* nwenode = pos->next->next;

		free(pos->next);
		pos->next = NULL;
		pos->next = nwenode;
		
	}
	else
	{
		printf("后面没有元素可删!\n");
	}
}

void SListDestory(SLTNode** pphead)//销毁结点
{
	assert(pphead);

	SLTNode* cur = *pphead;
	while (cur)
	{
		SLTNode* next = cur->next;
		free(cur);

		cur = next;
	}
	*pphead = NULL;
}