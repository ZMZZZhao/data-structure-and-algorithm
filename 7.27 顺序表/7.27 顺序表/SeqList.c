#include"SeqList.h"

void SeqListInit(SLT* psl)
{
	assert(psl);
	psl->a = NULL;
	psl->size = psl->capacity = 0;
}

void SeqListDestory(SLT* psl)
{
	assert(psl);

	if (psl->a)
	{
		free(psl->a);
		psl->a=NULL;
	}
	psl->size = psl->capacity = 0;
}

void SeqListPrint(SLT* psl)
{
	assert(psl);
	for (int i = 0; i < psl->size; ++i)
	{
		printf("%d ", psl->a[i]);
	}
	printf("\n");
}

void SeqListCheckCapcity(SLT* psl)
{
	assert(psl);

	// 是否满了，满了就要增容
	if (psl->size == psl->capacity)
	{
		size_t newcapacity = psl->capacity == 0 ? 4 : psl->capacity * 2;
		psl->a = realloc(psl->a, newcapacity * sizeof(SQDataType));
		psl->capacity = newcapacity;
	}
}

void SeqListPushBack(SLT* psl, SQDataType x)//尾插
{
	assert(psl);
    SeqListCheckCapcity(psl);

	psl->a[psl->size] = x;
	psl->size++;
}
void SeqListPushFront(SLT* psl, SQDataType x)//头插
{
	assert(psl);

	SeqListCheckCapcity(psl);
	//挪动数据
	int end = psl->size - 1;
	while (end >= 0)
	{
		psl->a[end + 1] = psl->a[end];
		end--;
	}
	psl->a[0] = x;
	psl->size++;
}
void SeqListPopBack(SLT* psl)//尾删
{
	assert(psl->size>0);//尾删>0断言
	psl->size--;
}
void SeqListPopFront(SLT* psl)//头删
{
	assert(psl->size>0);//尾删>0断言
	psl->a[0] = 0;
	for (int i = 0; i < psl->size; i++)
	{
		psl->a[i]=psl->a[i + 1];

	}
	psl->size--;
}

int ListFind(SLT* psl, SQDataType x)//判断存在某一元素
{
	assert(psl);
	for (int i = 0; i < psl->size; ++i)
	{
		if (psl->a[i] == x)
			printf("查找元素位置在：%d\n",i+1);
		
	}
	return -1;
}
void SqwListInsert(SLT* psl, size_t pos, SQDataType x)//插入某一元素
{
	assert(psl);
	assert(pos<=psl->size&&pos>=0);

	SeqListCheckCapcity(psl);

    size_t end = psl->size;
	while (end > pos)
	{
		psl->a[end] = psl->a[end-1];
		end--;
	}
	psl->a[pos] = x;
	psl->size++;

}
void SqeListErase(SLT* psl, size_t pos)//删除某一元素
{
	assert(psl);
	assert(pos < psl->size);

	size_t begin = pos + 1;
	while (begin < psl->size)
	{
		psl->a[begin-1] = psl->a[begin];
		begin++;
	}
	psl->size--;
}


size_t SqeListSize(SLT* psl)//查看数据内容
{
	assert(psl);
	return psl->size;
}

int SqeListAt(SLT* psl,size_t pos, SQDataType x)//修改元素内容
{
	assert(psl);
	assert(pos < psl->size);
	psl->a[pos] = x;


}