

#include"SeqList.h"
void TestSeqList1()
{
	SLT s;//顺序表的结构体对象
	
	SeqListInit(&s);//初始化
	//尾插
	SeqListPushBack(&s, 1);
	SeqListPushBack(&s, 2);
	SeqListPushBack(&s, 3);
	SeqListPushBack(&s, 4);
	SeqListPushBack(&s, 5);
	SeqListPushBack(&s, 6);
	
	SeqListPrint(&s);//打印

	//头插
	SeqListPushFront(&s, -1);
	SeqListPrint(&s);
	//尾删
	SeqListPopBack(&s);
	SeqListPrint(&s);
	//头删
	SeqListPopFront(&s);
	SeqListPopFront(&s);
	SeqListPopFront(&s);

	SeqListPrint(&s);

    ListFind(&s,4);//判断某一元素
	SeqListPrint(&s);

    SqwListInsert(&s, 3, 6);//插入某一元素
	SeqListPrint(&s);

	SqeListErase(&s, 2);//删除某一元素
	SeqListPrint(&s);

	SqeListSize(&s);//查看数据内容
	
	SqeListAt(&s, 2, 0);//修改元素内容
	SeqListPrint(&s);

	SeqListDestory(&s);//释放
}

int main()
{
	TestSeqList1();//调用

	return 0;
}


// 菜单程序非常不方便调试分析问题
// 建议先写函数，先测试，测试基本没有问题，最后再写菜单
