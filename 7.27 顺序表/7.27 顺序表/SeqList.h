#define _CRT_SECURE_NO_WARNINGS
#pragma once
#include<stdio.h>
#include <stdlib.h>
#include <assert.h>
//静态的顺序表结构
//#define N 1000
//typedef int SQDataType;
//
//typedef struct SeqList
//{
//	SQDataType a[N];
//	int size;
//
//}SLT;
//typedef struct SeqList SLT;

//动态的顺序表结构
typedef int SQDataType;

typedef struct SeqList
{
SQDataType* a;
int size;//有效数据的个数
int capacity;//容量空间大小
}SLT;

// 增删查改
void SeqListInit(SLT* psl);//初始化
void SeqListDestory(SLT* psl);//释放

void SeqListPrint(SLT* psl);//打印数组内容

void SeqListPushBack(SLT* psl, SQDataType x);//尾插   O(1)
void SeqListPushFront(SLT* psl, SQDataType x);//头插  O(n)
void SeqListPopBack(SLT* psl);//尾删  O(1 )
void SeqListPopFront(SLT* psl);//头删

int ListFind(SLT* psl, SQDataType x);//判断存在某一元素
void SqwListInsert(SLT* psl, size_t pos, SQDataType x);//查找某一元素
void SqeListErase(SLT* psl, size_t pos);//删除某一元素

size_t SqeListSize(SLT* psl);//查看数据内容
int SqeListAt(SLT* psl, size_t pos, SQDataType x);//修改元素内容
