#include"test.h"

void QueueInit(Queue* pq)//初始化
{
	assert(pq);
	pq->phead = pq->ptail = NULL;
}
void QueueDestory(Queue* pq)//释放
{
	assert(pq);

	QueueNode* cur = pq->phead;
	while (cur)
	{
		QueueNode* next = cur->next;
		free(cur);
		cur = next;
	}
	pq->phead = pq->ptail = NULL;
}
void QueuePush(Queue* pq, QDataType x)//队尾插
{
	assert(pq);
	QueueNode* newnode = (QueueNode*)malloc(sizeof(QueueNode));
	if (newnode == NULL)
	{
		printf("malloc fail\n");
		exit(-1);
	}
	newnode->data = x;
	newnode->next = NULL;

	if (pq->ptail == NULL)
	{
		pq->phead = pq->ptail = newnode;
	}
	else
	{
		pq->ptail->next = newnode;
		pq->ptail = newnode;
	}
}

	void QueuePop(Queue * pq)//队头删
	{
		assert(pq);
		assert(!QueueEmpty(pq));

		if (pq->phead->next == NULL)
		{
			free(pq->phead);
			pq->phead = pq->ptail = NULL;
		}
		else
		{
			QueueNode* next = pq->phead->next;
			free(pq->phead);
			pq->phead = next;
		}
	}
	int QueueSize(Queue* pq)
	{
		assert(pq);
		int n = 0;
		QueueNode* cur = pq->phead;
		while (cur)
		{
			n++;
			cur = cur->next;
		}
		return n;
	}

	QDataType QueueFront(Queue* pq)//取队头元素
	{
		assert(pq);
		assert(!QueueEmpty(pq));

		return pq->phead->data;
	}
	QDataType QueueBack(Queue* pq)//取队尾元素
	{
		assert(pq);
		assert(!QueueEmpty(pq));

		return pq->ptail->data;
	}

	bool QueueEmpty(Queue* pq)//判空
	{
		assert(pq);
		return pq->phead == NULL && pq->ptail == NULL;
	}